Title: Welcome
Description: Source based Linux distribution with deterministic package management and decentralized workflow.
CSS: /css/main.css

#{include head}

About
=====
  Exherbo aims to be a stable and flexible distribution for developers,
  tinkerers and people who need their distribution to help them be productive.
  Exherbo actually makes it fun for developers to develop, be it on their own
  code or the distribution itself.  A good set of tools, clear, up-front
  configuration and excellent support for decentralized development make the
  system work *for* instead of *against* the developer.

Features
========

* Source based Linux distribution with up-front configuration.

    Common tasks like package installation and updates follow a flexible yet deterministic and
    comprehensible path.

* Decentralised development done properly.

    Mixing in packages from unofficial sources or even packages that you wrote yourself is easy.

* For developers - [we expect users to take part in the development.](/docs/expectations.html)

    The first year alone we counted contributions from over 70 different people, who worked on
    packages (both pre-existing and new), documentation, etc. Since then, we have not only been able
    to attract more and more active users, but also to entrust them with more and more
    responsiblity.

* Native multiple architecture support ([multiarch](/docs/multiarch-pr.html)).

    Our multiarch concept allows for cross-compiling to different architectures while still being able
    to use the features and dependency resolution of the package manager more easily than any other
    available distro.

* Uses a couple of bits of code, and some ideas, from other open source projects, but the majority of code is original, not forked.

    Exherbo takes some ideas from other open source projects but it's our own ideas that really sets
    us apart and defines Exherbo.

* Uses the [Paludis](https://paludis.exherbo.org) package manager with a [custom built EAPI](/docs/eapi/exheres-for-smarties.html).

    Paludis is a fast and flexible package manager that allows us to quickly add new features
    targeted specifically at Exherbo. Many of the defining features of Exherbo are based on Paludis
    and the tight co-operation between the Exherbo and Paludis developers.

* Simple and safe account management
  The system manage users and groups so you don't have to worry about them.

* Tracking unwritten packages
  With the unwritten repository, packages that haven't been made can be tracked by the distribution itself, speeding up workflow.

* Alternatives
  If program Y does everything X does, but better, Exherbo can use Y instead of X in all cases.

Why
===

* Some of our ideas differ greatly from other distributions and it's much easier if we don't have to fight legacy code and ideas.
* We need the freedom to break things when necessary.

Goals
=====

* All design goals must be phrased in such a way that it is hard to use them
  as slogans to justify stupidity.
* The target users know what they are doing.
* Encourage users to take part in the development process and support them with good feedback for their contributions
* No interactivity requirement. Controllable, repeatable behaviour with
  up-front configuration.
* Flexibility where it makes sense to provide flexibility.
* No over-centralisation. Only widely used packages are to be in main
  repositories, and make the tools good enough to deal with lots of small
  third party repositories for random other apps.
* Be able to deliver features that other distributions consider too ambitious.

#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->

<!-- Local Variables: -->
<!-- fill-column: 100 -->
<!-- End: -->
